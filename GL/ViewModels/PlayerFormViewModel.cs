﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using GL.Models;

namespace GL.ViewModels
{
    public class PlayerFormViewModel
    {
        public Player player { get; set; }
    }
}